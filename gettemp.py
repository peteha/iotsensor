import Adafruit_DHT
import time

DHT_SENSOR = Adafruit_DHT.DHT22
DHT_PIN = 4
tt = time.time()


while True:
    humidity, temperature = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)

    if humidity is not None and temperature is not None:
        print("Time={0:1} Temp={1:0.1f}*C  Humidity={2:0.1f}%".format(tt, temperature, humidity))
    else:
        print("Failed to retrieve data from humidity sensor")
    time.sleep(30)